const express = require('express');
require('dotenv').config();
var bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
app.use(bodyParser.json());
app.use(cors());
var cars = require('./server/carsService')
// var carsFind = require('./server/carsFind');
// var carsFilter = require('./server/carsFilter');
// var carsPost = require('./server/carsPost');
// var carsGet = require('./server/carsGet');
// var carsPut = require('./server/carsPut');
// var carsDelete = require('./server/carsDelete');
// var chekUser = require('./middleware/chekUser');
app.use(express.json());
// app.get('/', chekUser, index);
app.get('/cars', cars.carsGet);
app.post('/cars', cars.carsPost);
app.get('/cars/:id', cars.carsGetbyId);
app.put('/cars/:id', cars.carsPut);
app.delete('/cars/:id', cars.carsDelete);
app.get('/cars/filter/:size', cars.carsFilter);

app.listen(process.env.PORT_API, () => {
  console.log(`Example app listening on port ${process.env.PORT_API}`);
});