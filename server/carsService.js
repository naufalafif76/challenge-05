var multiparty = require('multiparty');
const randomstring = require("randomstring");
const path = require('path');
const fs = require('fs');
// fs.unlink(filePath, callbackFunction)

const { tb_cars } = require("../models");

const carsGet = async function (req, res) {
  const getCars = await tb_cars.findAll().then(function (result) {
    return result;
  });
  res.setHeader("Content-Type", "application/json");
  res.writeHead(200);
  res.end(
    JSON.stringify({
      message: "Success",
      display_message: "List Success ",
      data: getCars,
    })
  );
}

const carsGetbyId = async function (req, res) {
  const getCarsbyId = await tb_cars
    .findOne({ where: { id: req.params.id } })
    .then(function (result) {
      return result;
    });
  res.setHeader("Content-Type", "application/json");
  res.writeHead(200);
  res.end(
    JSON.stringify({
      message: "Success",
      display_message: "List success",
      data: getCarsbyId,
    })
  );
}

const carsPost = async function carsPost(req, res) {
  var form = new multiparty.Form();
  form.parse(req, async function (err, fields, files) {
    var ext = path.extname(files.image[0].originalFilename);
    var objExt = ext.split(".")
    var filename = randomstring.generate(6);
    var readerStream = fs.createReadStream(files.image[0].path);
    var dest_file = path.join(process.env.IMAGES_DIRECTORY, filename + "." + objExt[objExt.length - 1]);
    var writerStream = fs.createWriteStream(dest_file);
    readerStream.pipe(writerStream);
    const postCars = await tb_cars.create({
      'name': fields.name[0],
      'price': fields.price[0],
      'size': fields.size[0],
      'image': filename + "." + objExt[objExt.length - 1]
    }).then(function (result) {
      return result;
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({
      message: "Success",
      display_message: "Upload Success ",
      data: postCars
    }));
  });
}

const carsPut = async function (req, res) {
  var form = new multiparty.Form();
  form.parse(req, async function (err, fields, files) {
    var ext = path.extname(files.image[0].originalFilename);
    var objExt = ext.split(".");
    var filename = randomstring.generate(6);
    var readerStream = fs.createReadStream(files.image[0].path);
    var dest_file = path.join(
      process.env.IMAGES_DIRECTORY,
      filename + "." + objExt[objExt.length - 1]
    );
    var writerStream = fs.createWriteStream(dest_file);
    readerStream.pipe(writerStream);
    const putCars = await tb_cars.update({
      'name': fields.name[0],
      'price': fields.price[0],
      'size': fields.size[0],
      'image': filename + "." + objExt[objExt.length - 1],
    },
    { where: { id: req.params.id } }
    ).then(function (result) {
      return result;
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(
      JSON.stringify({
        message: "Success",
        display_message: "Edit Success ",
        data: putCars,
      })
    );
  });
}

const carsDelete = async function (req, res) {
  const deleteCars = await tb_cars.destroy({ where: { id: parseInt(req.params.id) } }).then(function (result) {
    return result;
  });
  res.setHeader("Content-Type", "application/json");
  res.writeHead(200);
  res.end(
    JSON.stringify({
      message: "Success",
      display_message: "Delete success",
      data: deleteCars,
    })
  );
}

const carsFilter = async function (req, res) {
  const filterCars = await tb_cars.findAll({ where: { size: req.params.size } }).then(function (result) {
    return result;
  });
  res.setHeader("Content-Type", "application/json");
  res.writeHead(200);
  res.end(
    JSON.stringify({
      message: "Success",
      display_message: "List Success ",
      data: filterCars,
    })
  );
}

module.exports = {
  carsGet,
  carsGetbyId,
  carsPost,
  carsPut,
  carsDelete,
  carsFilter
}
