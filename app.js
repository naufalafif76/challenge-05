const express = require('express');
const path = require("path");
const fs = require("fs");
require('dotenv').config();
var fileType = require('./library/fileType');
var assets = path.join(__dirname, 'assets');
var images = path.join(__dirname, 'images');
const app = express()
const axios = require('axios');
const { response } = require('express');
// const session = require('express-session');

app.use(express.json());
// app.use(session({
//   secret: 'binar',
//   saveUninitialized: false,
//   resave: true,
//   cookie: {
//     maxAge: 7200000
//   }
// }));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', async (req, res) => {
  // let sess = req.session;
  // if (!sess.Token) { return res.redirect('/login'); }
  var config = {
    method: 'get',
    url: process.env.PORT_API2 + 'cars',
    headers: {}
  };
  var data = await axios(config).then(function (response) {
    return response.data.data;
  }).catch(function (error) {
    return error;
  });
  res.render('cars', {
    base_url: process.env.BASE_URL,
    data: data
  });
})

app.get('/filter/:size', async (req, res) => {
  // let sess = req.session;
  // if (!sess.Token) { return res.redirect('/login'); }
  var config = {
    method: 'get',
    url: process.env.PORT_API2 + 'cars/filter/' + req.params.size,
    headers: {}
  };
  var data = await axios(config).then(function (response) {
    return response.data.data;
  }).catch(function (error) {
    return error
  });
  res.render('cars', {
    base_url: process.env.BASE_URL,
    data: data
  });
})

app.get('/create', (req, res) => {
  // let sess = req.session;
  // if (!sess.Token) { return res.redirect('/login'); }
  res.render('carsCreate', {
    base_url: process.env.BASE_URL,
    data: ""
  });
})

app.get('/edit/:id', async (req, res) => {
  // let sess = req.session;
  // if (!sess.Token) { return res.redirect('/login'); }
  var config = {
    method: 'get',
    url: process.env.PORT_API2 + 'cars/' + req.params.id,
    headers: {}
  };
  var data = await axios(config).then(function (response) {
    return response.data.data;
  }).catch(function (error) {
    return error
  });
  res.render('carsEdit', {
    base_url: process.env.BASE_URL,
    data: data
  });
})

// app.get('/login', (req, res) => {
//   let sess = req.session;
//   if (!sess.Token) {
//     res.render('login', {
//       base_url: process.env.BASE_URL,
//       data: ""
//     });
//   } else {
//     return res.redirect('/');
//   }
// })

// app.get('/submit_login', async (req, res) => {
//   var checkLogin = await axios.get(process.env.PORT_API + 'login?username=' + req.query.username).then(function (response) {
//     return response;
//   })
//   if (checkLogin.data.message == "Success") {
//     req.session.Token = "binar123";
//     res.setHeader("Content-Type", "application/json");
//     res.writeHead(200);
//     res.end(JSON.stringify({
//       message: "Success",
//       display_message: checkLogin.data.display_message,
//       data: "binar1234"
//     }));
//   } else {
//     res.setHeader("Content-Type", "application/json");
//     res.writeHead(200);
//     res.end(JSON.stringify({
//       message: "Failed",
//       display_message: checkLogin.data.display_message,
//       data: "binar1234"
//     }));
//   }
// })

app.get('/images/:file', (req, res) => {
  var file = req.params.file;
  var extname = path.extname(file);
  var targetfile = path.join(images, file);
  fs.readFile(targetfile, function (error, content) {
    res.writeHead(200, {
      'Content-Type': fileType(extname.replace(".", ""))
    });
    res.end(content, 'utf-8');
  });
})

app.get('/assets/:file', (req, res) => {
  var file = req.params.file;
  var extname = path.extname(file);
  var targetfile = path.join(assets, file);
  fs.readFile(targetfile, function (error, content) {
    res.writeHead(200, {
      'Content-Type': fileType(extname.replace(".", ""))
    });
    res.end(content, 'utf-8');
  });
})

app.listen(process.env.PORT_WEB, () => {
  console.log(`Example app listening on port ${process.env.PORT_WEB}`)
})